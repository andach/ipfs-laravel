# IPFS Laravel Library

This is a simple library to access IPFS services in Laravel. It works via use of the HTTP API to give simple access to IPFS functions including navigating content, retreiving pictures and files, and adding text and files to the IPFS network. 

## Installing

This library requires the cURL module:

```bash
$ sudo apt-get install php7.2-curl
$ composer install andach/ipfs-laravel
$ php artisan vendor:publish
```

Please see /config/ipfs.php in your app to configure the API server and relevant ports. It is strongly recommended to run a local IPFS Instance. In Ubuntu Linux, you will be able to run `sudo apt install ipfs` to get a compatible version. 

```PHP
use Andach\LaravelIPFS\IPFS;

// connect to local daemon API server
$ipfs = new IPFS("localhost", "8080", "5001");
```

Note that if you are using a Docker container via docker-compose (please see docker-compose.sample.yml for an example that should work with a simple Laravel project using an SQL database), then you will need to use "ipfs" rather than "localhost" as the server name. 

## API
### add

Adds content to IPFS. 

```PHP
$hash = $ipfs->add("Hello world");
var_dump($hash); // QmfM2r8seH2GiRaC4esTjeraXEachRt8ZsSeGaWTPLyMoG
```

#### cat

Retrieves the contents of a single hash.

```PHP
$ipfs->cat('Qmbgm7f1uTESdUdddaSeV3mMT5nSQ7GijN2Hb7nCRj6oTe');
```

#### ls
Gets the node structure of a hash.

```PHP
$obj = $ipfs->ls($hash);

foreach ($obj as $e) {
	echo $e['Hash'];
	echo $e['Size'];
	echo $e['Name'];
}
```

#### pin

Pin or unpin a hash.

```PHP
$ipfs->pinAdd($hash);
$ipfs->pinRm($hash);
```

#### size

Returns object size.

```PHP
$size = $ipfs->size($hash);
```

#### Pin

Get information about your ipfs node.

```PHP
print_r($ipfs->id());
```