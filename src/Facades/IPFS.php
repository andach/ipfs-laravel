<?php

namespace Andach\LaravelIPFS\Facades;

use Illuminate\Support\Facades\Facade;

class IPFS extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'ipfs'; }
}