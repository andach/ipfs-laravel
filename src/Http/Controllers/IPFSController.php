<?php

namespace Andach\LaravelIPFS\Controllers;

use Andach\LaravelIPFS\IPFS;
use Illuminate\Http\Request;

class IPFSController extends Controller
{
    public function hash($hash)
    {
        //TODO: Understand how this works with the IP address and Docker. Will this have to be changed a lot? 
        $ipfs = new IPFS("192.168.64.4", "8080", "5001");

        $img = $ipfs->cat($hash);
        return response($img)->header('Content-type', 'image/png');
    }
}
