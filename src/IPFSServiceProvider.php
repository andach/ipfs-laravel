<?php

namespace Andach\LaravelIPFS;

use Illuminate\Support\ServiceProvider;

class IPFSServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'../config/ipfs.php' => config_path('ipfs.php'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ipfs', function () {
            return new IPFS(config('ipfs.ip'), config('ipfs.port'), config('ipfs.apiport'));
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [IPFS::class];
    }
}
