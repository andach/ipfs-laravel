<?php

namespace Andach\LaravelIPFS\Tests;

require_once __DIR__ . '/../vendor/autoload.php';

use Andach\IPFS\IPFS;

class IPFSTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ipfs
     */
    public $ipfs;

    public function setUp()
    {
        $this->ipfs = new IPFS(config('ipfs.ip'), config('ipfs.port'), config('ipfs.apiport'));
    }

    public function tearDown()
    {
        $this->ipfs = null;
    }
}
